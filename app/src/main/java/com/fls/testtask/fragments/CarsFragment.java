package com.fls.testtask.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.fls.testtask.R;
import com.fls.testtask.api.CarApi;
import com.fls.testtask.api.response.CarApiResponse;
import com.fls.testtask.model.Car;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CarsFragment extends BaseTabFragment implements SwipeRefreshLayout.OnRefreshListener {

    private static final String ARG_CARS_LIST = "arg:cars_list";

    private RecyclerView mCarRecycler;
    private CarRecyclerAdapter mCarAdapter;
    private ProgressDialog mProgress;
    private SwipeRefreshLayout mSwipeRefresh;

    public static CarsFragment newInstance() {

        Bundle args = new Bundle();

        CarsFragment fragment = new CarsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tab_cars, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mCarRecycler = (RecyclerView) view.findViewById(R.id.fragment_cars_recycler);
        mCarRecycler.setLayoutManager(new LinearLayoutManager(getContext()));

        mCarAdapter = new CarRecyclerAdapter(new ArrayList<Car>());
        mCarRecycler.setAdapter(mCarAdapter);

        mSwipeRefresh = (SwipeRefreshLayout) view.findViewById(R.id.fragment_cars_refresh);
        mSwipeRefresh.setOnRefreshListener(this);

        mProgress = new ProgressDialog(getActivity());
        mProgress.setTitle(getString(R.string.dialog_loading));
        mProgress.setMessage(getString(R.string.dialog_please_wait));
        mProgress.setCancelable(false);
        if (savedInstanceState != null && savedInstanceState.containsKey(ARG_CARS_LIST)) {
            ArrayList<Car> list = (ArrayList<Car>) savedInstanceState.getSerializable(ARG_CARS_LIST);
            savedInstanceState.remove(ARG_CARS_LIST);
            mCarAdapter.setItems(list);
            mCarAdapter.notifyDataSetChanged();
        } else {
            loadCars(true);
        }
    }

    private void showProgressDialog() {
        mProgress.show();
    }

    private void hideProgressDialog() {
        if (mProgress.isShowing())
            mProgress.hide();
    }

    private void hideSwipeRefresh() {
        if (mSwipeRefresh.isRefreshing())
            mSwipeRefresh.setRefreshing(false);
    }


    private void loadCars(boolean showDialog) {
        Log.i(LOG_TAG, "Trying to loading cars");
        if (showDialog) {
            showProgressDialog();
        }
        CarApi.CarsService carService = CarApi.getService();
        carService.getCars().enqueue(new Callback<CarApiResponse>() {
            @Override
            public void onResponse(Call<CarApiResponse> call, Response<CarApiResponse> response) {
                Log.i(LOG_TAG, "Cars loaded");
                mCarAdapter.setItems(response.body().getCars());
                mCarAdapter.notifyDataSetChanged();
                hideProgressDialog();
                hideSwipeRefresh();
            }

            @Override
            public void onFailure(Call<CarApiResponse> call, Throwable t) {
                Log.e(LOG_TAG, t.getMessage());
                Toast.makeText(getActivity(), R.string.text_reload_page, Toast.LENGTH_SHORT).show();
                hideProgressDialog();
                hideProgressDialog();
            }
        });
    }


    @Override
    public int getTabTitle() {
        return R.string.tab_cars_name;
    }

    @Override
    public void onRefresh() {
        mSwipeRefresh.setRefreshing(true);
        loadCars(false);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(ARG_CARS_LIST, mCarAdapter.getItems());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mProgress != null) {
            mProgress.dismiss();
        }
    }

    class CarRecyclerAdapter extends RecyclerView.Adapter<CarRecyclerAdapter.CarViewHolder> {

        private ArrayList<Car> mItems;

        public CarRecyclerAdapter(ArrayList<Car> items) {
            mItems = items;
        }

        public ArrayList<Car> getItems() {
            return mItems;
        }

        public void setItems(ArrayList<Car> items) {
            mItems = items;
        }

        @Override
        public CarViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fargment_cars_recycler_item, parent, false);
            return new CarViewHolder(view);
        }

        @Override
        public void onBindViewHolder(CarViewHolder holder, int position) {
            Car car = mItems.get(position);
            holder.bindCar(car);
        }

        @Override
        public int getItemCount() {
            return mItems.size();
        }

        class CarViewHolder extends RecyclerView.ViewHolder {
            private TextView mModel;
            private TextView mYear;
            private TextView mColor;
            private TextView mPrice;

            public CarViewHolder(View itemView) {
                super(itemView);
                mModel = (TextView) itemView.findViewById(R.id.car_model);
                mYear = (TextView) itemView.findViewById(R.id.car_year);
                mColor = (TextView) itemView.findViewById(R.id.car_color);
                mPrice = (TextView) itemView.findViewById(R.id.car_price);
            }

            public void bindCar(Car car) {
                mModel.setText(car.getModel());
                mYear.setText(car.getYear());
                mColor.setBackgroundColor(car.getColor());
                mPrice.setText(String.valueOf(car.getPrice()));
            }
        }
    }
}
