package com.fls.testtask.fragments;

import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;

public abstract class BaseTabFragment extends Fragment {

    protected final String LOG_TAG = getClass().getSimpleName();

    @StringRes
    public abstract int getTabTitle();

}
