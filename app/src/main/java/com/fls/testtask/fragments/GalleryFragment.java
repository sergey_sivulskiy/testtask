package com.fls.testtask.fragments;

import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.fls.testtask.R;
import com.fls.testtask.model.GalleryImage;
import com.fls.testtask.utils.NoTransformation;
import com.fls.testtask.utils.TransformAndSaveToCache;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class GalleryFragment extends BaseTabFragment {

    private final String FILE_TYPE = ".jpg";

    private RecyclerView mGalleryRecycler;
    private GalleryRecyclerAdapter mGalleryAdapter;

    public static GalleryFragment newInstance() {

        Bundle args = new Bundle();

        GalleryFragment fragment = new GalleryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tab_gallery, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mGalleryRecycler = (RecyclerView) view.findViewById(R.id.fragment_gallery_recycler);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 2);
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (position % 2 == 0)
                    return 1;
                else
                    return 2;
            }
        });
        mGalleryRecycler.setLayoutManager(gridLayoutManager);
        mGalleryRecycler.setHasFixedSize(true);

        mGalleryAdapter = new GalleryRecyclerAdapter(getImages());
        mGalleryRecycler.setAdapter(mGalleryAdapter);

    }

    private List<GalleryImage> getImages() {
        List<GalleryImage> list = new ArrayList<>();
        File picturesDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        if (picturesDir.isDirectory() &&
                picturesDir.exists() &&
                picturesDir.canRead() &&
                picturesDir.list() != null) {
            for (File f : picturesDir.listFiles()) {
                if (!f.isDirectory() && f.getName().endsWith(FILE_TYPE)) {
                    list.add(new GalleryImage(f));
                }
            }
        }
        return list;
    }


    @Override
    public int getTabTitle() {
        return R.string.tab_gallery_name;
    }

    class GalleryRecyclerAdapter extends RecyclerView.Adapter<GalleryRecyclerAdapter.GalleryViewHolder> {

        private List<GalleryImage> mItems;

        public GalleryRecyclerAdapter(List<GalleryImage> items) {
            setItems(items);
        }

        public List<GalleryImage> getItems() {
            return mItems;
        }

        public void setItems(List<GalleryImage> items) {
            mItems = items;
        }

        @Override
        public GalleryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_gallery_recycler_item, parent, false);
            return new GalleryViewHolder(view);
        }

        @Override
        public void onBindViewHolder(GalleryViewHolder holder, int position) {
            GalleryImage galleryImage = mItems.get(position);
            holder.bindGalleryImage(galleryImage);
        }

        @Override
        public int getItemCount() {
            return mItems.size();
        }

        class GalleryViewHolder extends RecyclerView.ViewHolder {

            private ImageView mImageView;

            public GalleryViewHolder(View itemView) {
                super(itemView);
                mImageView = (ImageView) itemView.findViewById(R.id.fragment_gallery_image_view);
            }

            public void bindGalleryImage(GalleryImage image) {
                File cacheDir = getContext().getCacheDir();
                File sourceImageFile = image.getFile();

                boolean isCache = false;

                if (cacheDir != null && cacheDir.listFiles() != null) {
                    for (File file : cacheDir.listFiles()) {
                        if ((sourceImageFile.getName() + TransformAndSaveToCache.THUMB_POSTFIX).equals(file.getName())) {
                            isCache = true;
                        }
                    }
                }

                Uri uri = isCache ?
                        Uri.fromFile(new File(cacheDir, sourceImageFile.getName().concat(TransformAndSaveToCache.THUMB_POSTFIX))) :
                        Uri.fromFile(image.getFile());
                Picasso.with(getActivity())
                        .load(uri)
                        .transform(isCache ? new NoTransformation() : new TransformAndSaveToCache(sourceImageFile.getName(), cacheDir))
                        .into(mImageView);

            }
        }
    }
}
