package com.fls.testtask.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fls.testtask.R;
import com.fls.testtask.views.PaintView;

public class PaintFragment extends BaseTabFragment {

    private PaintView mPaintView;

    public static PaintFragment newInstance() {

        Bundle args = new Bundle();

        PaintFragment fragment = new PaintFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tab_paint, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPaintView = (PaintView) view.findViewById(R.id.fragment_paint_view);
        mPaintView.setColor(Color.RED);
        mPaintView.setBackgroundColor(Color.WHITE);
    }

    @Override
    public int getTabTitle() {
        return R.string.tab_paint_name;
    }
}
