package com.fls.testtask.model;


import java.io.File;

public class GalleryImage {

    private File mFile;

    public GalleryImage(File mFile) {
        this.mFile = mFile;
    }


    public File getFile() {
        return mFile;
    }
}
