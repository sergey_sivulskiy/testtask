package com.fls.testtask.model;

import android.graphics.Color;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Car implements Serializable {

    @SerializedName("model")
    private String mModel;

    @SerializedName("year")
    private String mYear;

    @SerializedName("color")
    private String mColor;

    @SerializedName("price")
    private int mPrice;

    public String getModel() {
        return mModel;
    }

    public String getYear() {
        return mYear;
    }

    public int getColor() {
        switch (mColor) {
            case "black":
                return Color.BLACK;
            case "blue":
                return Color.BLUE;
            case "white":
                return Color.WHITE;
            case "yellow":
                return Color.YELLOW;
            default:
                return Color.BLACK;
        }
    }

    public int getPrice() {
        return mPrice;
    }
}
