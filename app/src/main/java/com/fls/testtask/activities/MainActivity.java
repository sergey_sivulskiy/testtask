package com.fls.testtask.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import com.fls.testtask.R;
import com.fls.testtask.fragments.BaseTabFragment;
import com.fls.testtask.fragments.CarsFragment;
import com.fls.testtask.fragments.GalleryFragment;
import com.fls.testtask.fragments.PaintFragment;
import com.fls.testtask.views.CustomViewPager;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String LOG_TAG = MainActivity.class.getSimpleName();

    private static final int REQUEST_PERMISSIONS = 1;

    private CustomViewPager mViewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            final String[] permissions = new String[]{
                    Manifest.permission.READ_EXTERNAL_STORAGE,
            };

            List<String> requestPermissionsList = new LinkedList<>();

            for (String p : permissions) {
                if (ContextCompat.checkSelfPermission(this, p) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissionsList.add(p);
                }
            }

            if (!requestPermissionsList.isEmpty()) {
                ActivityCompat.requestPermissions(
                        this,
                        requestPermissionsList.toArray(new String[requestPermissionsList.size()]),
                        REQUEST_PERMISSIONS);
            } else {
                init();
            }
        } else {
            init();
        }
    }

    private void init() {
        setContentView(R.layout.activity_main);

        mViewPager = (CustomViewPager) findViewById(R.id.fragment_tabs_viewpager);
        mViewPager.setSwipeEnable(false);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.fragment_tabs_tablayout);

        PagerAdapter pagerAdapter = new PagerAdapter(getSupportFragmentManager());
        pagerAdapter.addFragment(CarsFragment.newInstance());
        pagerAdapter.addFragment(PaintFragment.newInstance());
        pagerAdapter.addFragment(GalleryFragment.newInstance());

        mViewPager.setAdapter(pagerAdapter);
        tabLayout.setupWithViewPager(mViewPager);
    }

    private class PagerAdapter extends FragmentStatePagerAdapter {
        private List<BaseTabFragment> mFragments = new ArrayList<>();

        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public BaseTabFragment getItem(int position) {
            return mFragments.get(position);
        }


        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return getString(mFragments.get(position).getTabTitle());
        }

        public void addFragment(BaseTabFragment fragment) {
            mFragments.add(fragment);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSIONS: {
                for (int g : grantResults) {
                    if (g != PackageManager.PERMISSION_GRANTED) {
                        new AlertDialog.Builder(this)
                                .setTitle(R.string.text_error)
                                .setMessage(R.string.text_permission_not_granted)
                                .setCancelable(true)
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        finish();
                                    }
                                })
                                .show();
                        return;
                    }
                }

                init();
                return;
            }
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
