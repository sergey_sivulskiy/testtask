package com.fls.testtask.api;

import com.fls.testtask.api.response.CarApiResponse;
import com.fls.testtask.model.Car;
import com.google.gson.Gson;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Part;

public class CarApi {
    private static final String BASE_URL = "https://dl.dropboxusercontent.com/s/6bcldgdhxa6mlk3/";


    private static final CarsService SERVICE = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(CarsService.class);

    public static CarsService getService() {
        return SERVICE;
    }

    public interface CarsService {
        @GET("cars.json")
        Call<CarApiResponse> getCars();
    }
}
