package com.fls.testtask.api.response;

import com.fls.testtask.model.Car;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CarApiResponse {

    @SerializedName("cars")
    private ArrayList<Car> mCars;

    public ArrayList<Car> getCars() {
        return mCars;
    }
}
