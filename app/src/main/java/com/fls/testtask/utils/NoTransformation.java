package com.fls.testtask.utils;

import android.graphics.Bitmap;

import com.squareup.picasso.Transformation;

public class NoTransformation implements Transformation {
    @Override
    public Bitmap transform(Bitmap source) {
        return source;
    }

    @Override
    public String key() {
        return "noTransform";
    }
}
