package com.fls.testtask.utils;

import android.graphics.Bitmap;
import android.media.ThumbnailUtils;

import com.squareup.picasso.Transformation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class TransformAndSaveToCache implements Transformation {

    public static final String THUMB_POSTFIX = ".thumb";
    private static final int THUMB_WIDTH = 150;
    private static final int THUMB_HEIGHT = 150;

    private String mFileName;
    private File mSaveDir;

    public TransformAndSaveToCache(String fileName, File saveDir) {
        mFileName = fileName;
        mSaveDir = saveDir;
    }

    @Override
    public Bitmap transform(Bitmap source) {
        Bitmap thumb = ThumbnailUtils.extractThumbnail(source, THUMB_WIDTH, THUMB_HEIGHT);
        if (!source.isRecycled()) {
            source.recycle();
        }

        File file = new File(mSaveDir, mFileName.concat(THUMB_POSTFIX));
        try {
            FileOutputStream fout = new FileOutputStream(file);
            thumb.compress(Bitmap.CompressFormat.JPEG, 100, fout);
            fout.flush();
            fout.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return thumb;
    }

    @Override
    public String key() {
        return "transform";
    }
}
